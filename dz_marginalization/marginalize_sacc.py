#!/usr/bin/python3

from dz_marginalization import DzMarg
import sacc

sigma_dz = {
    "DESgc__0": 0.007,
    "DESgc__1": 0.007,
    "DESgc__2": 0.006,
    "DESgc__3": 0.01,
    "DESgc__4": 0.01,
    "DESwl__0": 0.016,
    "DESwl__1": 0.013,
    "DESwl__2": 0.011,
    "DESwl__3": 0.022,
    "eBOSS__0": 0,
    "eBOSS__1": 0,
    "DECALS__0": 0.007,
    "DECALS__1": 0.007,
    "DECALS__2": 0.006,
    "DECALS__3": 0.01,
    "KiDS1000__0": 0.0106,
    "KiDS1000__1": 0.0113,
    "KiDS1000__2": 0.0118,
    "KiDS1000__3": 0.0087,
    "KiDS1000__4": 0.0097,
}

mean_dz = {
    "DESgc__0": 0.,
    "DESgc__1": 0.,
    "DESgc__2": 0.,
    "DESgc__3": 0.,
    "DESgc__4": 0.,
    "DESwl__0": -0.001,
    "DESwl__1": -0.019,
    "DESwl__2": 0.009,
    "DESwl__3": -0.018,
    "eBOSS__0": 0.,
    "eBOSS__1": 0.,
    "DECALS__0": 0.,
    "DECALS__1": 0.,
    "DECALS__2": 0.,
    "DECALS__3": 0.,
    "KiDS1000__0": 0.,
    "KiDS1000__1": -0.002,
    "KiDS1000__2": -0.013,
    "KiDS1000__3": -0.011,
    "KiDS1000__4": 0.006,
}

bzi = {
    "DESgc__0": 1.48,
    "DESgc__1": 1.76,
    "DESgc__2": 1.78,
    "DESgc__3": 2.19,
    "DESgc__4": 2.23,
    "eBOSS__0": 2.1,
    "eBOSS__1": 2.5,
    "DECALS__0": 1.13,
    "DECALS__1": 1.40,
    "DECALS__2": 1.35,
    "DECALS__3": 1.77,
}


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Compute the N(z) marginalization")
    parser.add_argument('-ddz', '--ddz', type=float, default=0.001, help='z shift for dz differentiation')
    args = parser.parse_args()

    ddz = args.ddz

    s = sacc.Sacc.load_fits("/mnt/extraspace/gravityls_3/sacc_files/cls_FD_covG.fits")
    marg = DzMarg(s, ddz, sigma_dz, bzi, mean_dz=mean_dz, outprefix="cls_FD_covG",
                  outdir='./output/nz_shifted/')
    marg.write_sacc()

