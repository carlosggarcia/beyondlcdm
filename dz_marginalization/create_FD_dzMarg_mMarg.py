#!/usr/bin/python3.8

import sacc
import numpy as np


def add_marg_cov(s, sm):
    cov = s.covariance.dense

    s_ix = []
    sm_ix = []
    for trs in s.get_tracer_combinations():
        sm_ixi = sm.indices(tracers=trs)
        s_ixi = s.indices(tracers=trs)
        if len(sm_ixi) == 0:
            continue
        elif len(sm_ixi) != len(s_ixi):
            raise RuntimeError(f"Number of elements for {trs} does not agree between sacc files: {len(sm_ixi)} vs {len(s_ixi)}")
        s_ix.extend(s_ixi)
        sm_ix.extend(sm_ixi)

    cov[np.ix_(s_ix, s_ix)] += sm.covariance.covmat[sm_ix][:, sm_ix]

    s.add_covariance(cov, overwrite=True)


def check_cov(sm, FD, FDm):
    """
    Check that all blocks are ok
    """
    for trs in sm.get_tracer_combinations():
        FD_ix = FD.indices(tracers=trs)
        sm_ix = sm.indices(tracers=trs)
        dt = sm.get_data_types(tracers=trs)
        if len(FD_ix) == 0:
            FD_ix = FD.indices(tracers=trs[::-1])
        if len(FD_ix) == 0:
            continue
        for trs2 in sm.get_tracer_combinations():
            FD_ix2 = FD.indices(tracers=trs2)
            sm_ix2 = sm.indices(tracers=trs2)
            dt2 = sm.get_data_types(tracers=trs2)
            if len(FD_ix2) == 0:
                FD_ix2 = FD.indices(tracers=trs2[::-1])
            if len(FD_ix2) == 0:
                continue

            assert np.all(FDm.covariance.dense[FD_ix][:, FD_ix2] == FD.covariance.dense[FD_ix][:, FD_ix2] + sm.covariance.covmat[sm_ix][:, sm_ix2])


def main(no_dzMarg=False):
    if no_dzMarg:
        FD = sacc.Sacc.load_fits('/mnt/extraspace/gravityls_3/sacc_files/cls_FD_covG.fits')
    else:
        FD = sacc.Sacc.load_fits('./output/nz_shifted/cls_FD_covG_cls_NzMarg_cov_p0.00075.fits')
    SDm = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/desy1_ebossqso_cmbk_run1_4096/clsFid_covmMarg_with_fid.fits')
    NDm = sacc.Sacc.load_fits('/mnt/extraspace/davidjamiecarlos/dels_k1000_ebossqso_cmbk_run1_4096_binningasdamonge/clsFid_covmMarg_with_fid.fits')

    # Rename new tracer names to oldstyle
    renames = [("DESY1gc", "DESgc"), ("DESY1wl", "DESwl"), ("Planck__kappa18", "PLAcv"), ("SDSS__QSO", "eBOSS__"), ("DELS", "DECALS")]

    for nold, nnew in renames:
        for s in [SDm, NDm]:
            trnames = list(s.tracers.keys())
            for trn in trnames:
                if nold not in trn:
                    continue
                trn_new = trn.replace(nold, nnew)
                s.rename_tracer(trn, trn_new)

    # Remove reduncdant tracers and unwanted data types
    dttoremove = ["cl_0b", "cl_eb", "cl_be", "cl_bb"] if not no_dzMarg else []
    for dt in dttoremove :
        SDm.remove_selection(data_type=dt)
        NDm.remove_selection(data_type=dt)
    NDm.remove_tracers(['eBOSS__0', 'eBOSS__1'])

    print((FD.get_tracer_combinations()))
    print()
    print((SDm.get_tracer_combinations()))
    print()
    print((NDm.get_tracer_combinations()))

    FDm = FD.copy()
    print("Adding m-marginalized covariance for SD")
    add_marg_cov(FDm, SDm)
    print("Adding m-marginalized covariance for ND")
    add_marg_cov(FDm, NDm)

    # Check everything is consistent with previous FD sacc file
    # Mean should have not changed
    assert np.all(FDm.mean == FD.mean)

    # Covariance should be different for wl probes
    assert not np.all(FDm.covariance.dense == FD.covariance.dense)
    FD_ix = FDm.indices(data_type='cl_00')
    assert np.all(FDm.covariance.dense[FD_ix][:, FD_ix] == FD.covariance.dense[FD_ix][:, FD_ix])
    FD_ix = FDm.indices(data_type='cl_0e')
    assert not np.all(FDm.covariance.dense[FD_ix][:, FD_ix] == FD.covariance.dense[FD_ix][:, FD_ix])
    FD_ix = FDm.indices(data_type='cl_ee')
    assert not np.all(FDm.covariance.dense[FD_ix][:, FD_ix] == FD.covariance.dense[FD_ix][:, FD_ix])

    # Check blocks for autocov
    mean_th = []
    for trs in FD.get_tracer_combinations():
        FD_ix = FD.indices(tracers=trs)
        SD_ix = SDm.indices(tracers=trs)
        ND_ix = NDm.indices(tracers=trs)
        if len(SD_ix) == 0 and len(ND_ix) == 0:
            SD_ix = SDm.indices(tracers=trs[::-1])
            ND_ix = NDm.indices(tracers=trs[::-1])
        if len(SD_ix) != 0:
            mean_th.extend(SDm.mean[SD_ix])
            assert np.all(FDm.covariance.dense[FD_ix][:, FD_ix] == FD.covariance.dense[FD_ix][:, FD_ix] + SDm.covariance.covmat[SD_ix][:, SD_ix])
        elif len(ND_ix) != 0:
            mean_th.extend(NDm.mean[ND_ix])
            assert np.all(FDm.covariance.dense[FD_ix][:, FD_ix] == FD.covariance.dense[FD_ix][:, FD_ix] + NDm.covariance.covmat[ND_ix][:, ND_ix])
        else:
            raise RuntimeError(f"The tracers {trs} were not found in SD nor ND.")

    # Check all blocks
    print("Checking all cov blocks of SD")
    check_cov(SDm, FD, FDm)
    print("Checking all cov blocks of ND")
    check_cov(NDm, FD, FDm)

    # Covariance checked
    delta = FD.mean - mean_th
    icov = np.linalg.inv(FD.covariance.dense)
    icovm = np.linalg.inv(FDm.covariance.dense)
    print('Chi2 FD Cov old', delta.dot(icov).dot(delta))
    print('Chi2 FD Cov new', delta.dot(icovm).dot(delta))

    # Save the sacc file
    if no_dzMarg:
        fname = 'cls_FD_covG_mMarg.fits'
    else:
        fname = 'cls_FD_covG_dzMarg_mMarg.fits'
    FDm.save_fits(fname, overwrite=True)

    # Check it was written correctly
    FDm2 = sacc.Sacc.load_fits(fname)

    assert np.all(FDm2.mean == FDm.mean)
    assert np.all(FDm2.covariance.covmat == FDm.covariance.covmat)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Create the sacc file with covG and m and dz marginalized")
    parser.add_argument('-no_dzMarg', '--no_dzMarg', action="store_true", help='Add only mMarg, not the marginalized dz')
    args = parser.parse_args()

    main(args.no_dzMarg)
