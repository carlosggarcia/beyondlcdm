#!/usr/bin/python3

from scipy.special import jv
from scipy.interpolate import interp1d
from scipy.integrate import simps
import numpy as np
import matplotlib.pyplot as plt
import pyccl as ccl
import sacc
import sys
import os

class DzMarg():
    def __init__(self, sfile, ddz, sigma_dz, bzi, mean_dz=None, resample=None,
                 outdir='./', outprefix=''):
        self._cosmo = None
        self.sfile = s = sfile
        self.ddz = ddz
        self.outprefix = os.path.join(outdir, outprefix)
        self.resample = resample
        self.sigma_dz = sigma_dz
        self.bzi = bzi

        # The following has to be called after defining self.names and
        # self.dir_tracers
        self.dir_surveys = self._get_dir_surveys()

        s.remove_selection(data_type='cl_0b')
        s.remove_selection(data_type='cl_eb')
        s.remove_selection(data_type='cl_be')
        s.remove_selection(data_type='cl_bb')

        # Apply mean shift to the n(z)
        # dn/dzt = dzf/dzt|_zt * dn/dzf|_zt
        if mean_dz is not None:
            for trn, tr in self.sfile.tracers.items():
                if trn not in mean_dz:
                    continue
                nz = interp1d(tr.z, tr.nz, kind='cubic', bounds_error=False,
                              fill_value=0)
                tr.nz = nz(tr.z - mean_dz[trn])

        self.Tmat = None
        self.cv = None
        self.nz_marg = None

        self._base_ccl_tr = None
        self._base_cls_arr = None
        os.makedirs(outdir, exist_ok=True)

    def _get_dir_surveys(self):
        surveys = []
        for n in self.sfile.tracers.keys():
            survey = self._get_survey(n)
            if survey not in surveys:
                surveys.append(survey)

        return surveys

    def get_cosmo(self):
        if self._cosmo is None:
            self._cosmo = ccl.CosmologyVanillaLCDM()
        return self._cosmo

    def _get_dtype(self, tr1, tr2):
        q1 = self.sfile.tracers[tr1].quantity
        q2 = self.sfile.tracers[tr2].quantity
        dtype = 'cl_'

        for qi in [q1, q2]:
            if qi in ['galaxy_density', 'cmb_convergence']:
                dtype += '0'
            elif qi in 'galaxy_shear':
                dtype += 'e'
            else:
                raise ValueError('tracer type not implemented:', qi)

        if dtype == 'cl_e0':
            dtype = 'cl_0e'

        return dtype

    def _get_ccl_tracer(self, trname, dndz=None):
        tr = self.sfile.get_tracer(trname)

        cosmo = self.get_cosmo()
        if (dndz is None) and isinstance(tr, sacc.tracers.NZTracer):
            dndz = (tr.z, tr.nz)

        q = tr.quantity
        if q == 'galaxy_density':
            bias = (tr.z, np.ones_like(tr.z) * self.bzi[trname] if isinstance(self.bzi[trname], float) else self.bzi[trname](tr.z))
            tr_ccl = ccl.NumberCountsTracer(cosmo, dndz=dndz, bias=bias, has_rsd=False)
        elif q == 'galaxy_shear':
            tr_ccl = ccl.WeakLensingTracer(cosmo, dndz)
        elif q == 'cmb_convergence':
            tr_ccl = ccl.CMBLensingTracer(cosmo, z_source=1100)

        return tr_ccl

    def _get_base_ccl_tracers(self):
        if self._base_ccl_tr is None:
            print("Computing base tracers", flush=True)
            self._base_ccl_tr = {trn: self._get_ccl_tracer(trn) for trn in self.sfile.tracers.keys()}
        return self._base_ccl_tr

    def _get_base_cls_array(self):
        if self._base_cls_arr is None:
            ts = self._get_base_ccl_tracers()

            cls = []
            for n1, n2 in self.sfile.get_tracer_combinations():
                t1 = ts[n1]
                t2 = ts[n2]
                cl_binned = self._get_binned_cell(n1, n2, t1, t2)
                cls.append(cl_binned)
            self._base_cls_arr = np.concatenate(cls)

        return self._base_cls_arr

    def _get_binned_cell(self, trn1, trn2, tr1, tr2):
        cosmo = self.get_cosmo()
        l_sample = np.unique(np.geomspace(2., 31000., 400).astype(int)).astype(float)
        l_sample = np.concatenate((np.array([0.]), l_sample))

        dtype = self._get_dtype(trn1, trn2)
        ind = self.sfile.indices(dtype, (trn1, trn2))
        bpw = self.sfile.get_bandpower_windows(ind)

        cl = ccl.angular_cl(cosmo, tr1, tr2, l_sample)
        cl_unbinned = interp1d(l_sample, cl)(bpw.values)
        cl_binned = np.dot(bpw.weight.T, cl_unbinned)

        return cl_binned

    def compute_nz_response(self):
        ddz = self.ddz
        fname = self.outprefix + f'_T_p{ddz}.npz'

        if self.Tmat is not None:
            pass
        elif os.path.isfile(fname):
            print("Reading T matrix", flush=True)
            self.Tmat = np.load(fname)['Tmat']
        else:
            print("Computing T matrix", flush=True)
            # Copied from prelim_script/create_nz_response.py
            cosmo = self.get_cosmo()
            s = self.sfile

            base_ccl_tr =  self._get_base_ccl_tracers()
            base_cls_arr = self._get_base_cls_array()

            def get_cls(name, nz):
                cells = base_cls_arr.copy()
                for n1n2 in s.get_tracer_combinations():
                    n1, n2 = n1n2
                    print(n1, n2, flush=True)
                    if (n1 != name) and (n2 != name):
                        continue

                    if n1 == name:
                        t1 = self._get_ccl_tracer(n1, nz)
                    else:
                        t1 = base_ccl_tr[n1]

                    if n2 == name:
                        t2 = self._get_ccl_tracer(n2, nz)
                    else:
                        t2 = base_ccl_tr[n2]

                    cl_binned = self._get_binned_cell(n1, n2, t1, t2)

                    ix = s.indices(tracers=n1n2)
                    cells[ix] = cl_binned
                return cells

            Tmat = []
            for trn, tr in s.tracers.items():
                print(trn)
                if not isinstance(tr, sacc.tracers.NZTracer) or (self.sigma_dz[trn] == 0):
                    continue
                jacob = 1
                z, nz = tr.z, tr.nz
                # dn/dzt = dzf/dzt|_zt * dn/dzf|_zt
                nz = interp1d(z, nz, kind='cubic', bounds_error=False,
                              fill_value=0)
                # Add
                z_out = z + ddz
                nz_out = jacob * nz(z_out)
                clp = get_cls(trn, (z, nz_out))
                # Subtract
                z_out = z - ddz
                nz_out = jacob * nz(z_out)
                clm = get_cls(trn, (z, nz_out))
                Tmat.append((clp-clm)/(2*ddz))
            self.Tmat = np.array(Tmat)
            print(self.Tmat.shape)
            np.savez(fname, Tmat=self.Tmat)

        return self.Tmat

    def compute_covar_master(self):
        if self.cv is None:
            cov_diag = []
            for i, trn in enumerate(self.sfile.tracers.keys()):
                tr = self.sfile.tracers[trn]
                if not isinstance(tr, sacc.tracers.NZTracer) or (self.sigma_dz[trn] == 0):
                    continue
                cov_diag.append(self.sigma_dz[trn]**2)

            self.cv = np.diag(cov_diag)

        return self.cv

    def compute_nz_covar(self):
        fname = self.outprefix + f'_covNzMarg_p{self.ddz}.npz'

        if self.nz_marg is not None:
            pass
        elif os.path.isfile(fname):
            print("Reading marginalized covariance", flush=True)
            self.nz_marg = np.load(fname)['nz_marg']
        else:
            print("Computing marginalized covariance", flush=True)
            Tmat = self.compute_nz_response()
            cvN = self.compute_covar_master()

            self.nz_marg = np.dot(Tmat.T, np.dot(cvN, Tmat))
            np.savez(fname, nz_marg=self.nz_marg, cov_dz=cvN)

        return self.nz_marg

    def write_sacc(self):
        nz_marg = self.compute_nz_covar()

        # Use dense instead of covmat for compatibility with BlockDiagonal covs
        covar_old = self.sfile.covariance.dense
        covar_new = covar_old + nz_marg

        self.sfile.add_covariance(covar_new, overwrite=True)
        fname = self.outprefix + f'_cls_NzMarg_cov_p{self.ddz}.fits'
        print(f"Writing file to {fname}", flush=True)
        self.sfile.save_fits(fname, overwrite=True)

        return covar_new

    def _get_survey(self, name):
        return name.split('__')[0]
