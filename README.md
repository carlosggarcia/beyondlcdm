# BeyondLCDM

Beyond LCDM paper with data from https://github.com/xC-ell/growth-history

Summary slides: https://docs.google.com/presentation/d/1NY0H3zxoOCpKKtMRpxIde9HGj0fsBHASizT-fASHRdU/edit?usp=sharing

# Constraints with the following data combinations: 
- SD
- ND
- FD
- FD no clustering
- FD no shear

with

- linear scales only as in DESY1 ([1810.02499](https://arxiv.org/abs/1810.02499)); i.e. removing scales until `Delta chi2 < 1`) for the bestfit model.
- non linear scales with standard Halofit
- non linear scales with new Halofit

# Software: 
- hi_class mg_class
- CCL
- [MontePython](https://github.com/carlosggarcia/montepython_public/tree/beyondLCDM): Tests done on the BLCDM implementation:
  - test/test_ccl_blcdm.sh: Recover LCDM result with MG params = 0 (i.e. GR)
  - tests recover GR Pk inside montepython/ccl_class_blcdm.py
  - tested reproducing the BF Cells with notebook 
- [Cobaya lkl](https://github.com/xC-ell/xCell-likelihoods/tree/main/ClLike)
