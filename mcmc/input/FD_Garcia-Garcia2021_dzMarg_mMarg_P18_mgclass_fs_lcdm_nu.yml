# Configuration to use the Full Dataset (FD) from Garcia-Garcia et al. 2021
# (2105.12108), but with the dz and multiplicative biases marginalized into the
# covariance.

sampler:
  mcmc:
    burn_in: 0
    max_tries: 1000
    drag: True
      # covmat: 'input/FD_Garcia-Garcia2021_dzMarg_mMarg_P18_mgclass_fs_lcdm_nu.covmat2'

params:
  # BLCDM parameters
  # mu
  parameters_smg__1:
    prior:
      min: -10
      max: 10
    ref: 0.18
    proposal: 0.1
    latex: \mu_0 - 1

  # Sigma
  parameters_smg__2:
    prior:
      min: -10
      max: 10
    ref: -0.19
    proposal: 0.1
    latex: \Sigma_0 - 1

  # Cosmological parameters
  A_sE9:
    prior:
      min: 0.5
      max: 5.
    ref: 2.11
    latex: 10^9 A_s
    proposal: 0.01
    drop: True

  # Class does not understand A_sE9
  A_s:
    value: 'lambda A_sE9: A_sE9 * 1e-9'
    latex: A_s

  Omega_cdm:
    prior: 
      min: 0.07
      max: 0.83
    ref: 0.275
    latex: \Omega_c
    proposal: 0.01

  Omega_b:
    prior: 
      min: 0.03
      max: 0.07
    ref: 0.050
    latex: \Omega_b
    proposal: 0.001

  h: 
    prior: 
      min: 0.55
      max: 0.91
    ref: 0.665
    latex: h
    proposal: 0.01

  n_s:
    prior: 
      min: 0.87
      max: 1.07
    ref: 0.96
    latex: n_s
    proposal: 0.005

  tau_reio:
    prior:
      min: 0.004
      max: 1.000
    ref: 0.054
    proposal: 0.005

  sigma8:
    latex: \sigma_8

  S8:
    latex: S_8

  # Nuisance parameters
  # SD (DESgc + DESwl)
  bias_DESgc__0_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.41
      scale: 0.1
    latex: b^{DESgc_0}_1
    proposal: 0.1

  bias_DESgc__1_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.71
      scale: 0.1
    latex: b^{DESgc_1}_1
    proposal: 0.1

  bias_DESgc__2_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.69
      scale: 0.1
    latex: b^{DESgc_2}_1
    proposal: 0.1

  bias_DESgc__3_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 2.07
      scale: 0.1
    latex: b^{DESgc_3}_1
    proposal: 0.1

  bias_DESgc__4_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 2.14
      scale: 0.1
    latex: b^{DESgc_4}_1
    proposal: 0.1

  # ND (DECALS + KiDS)
  bias_DECALS__0_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.09
      scale: 0.1
    latex: b^{DECALS_0}_1
    proposal: 0.1

  bias_DECALS__1_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.34
      scale: 0.1
    latex: b^{DECALS_1}_1
    proposal: 0.1

  bias_DECALS__2_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.30
      scale: 0.1
    latex: b^{DECALS_2}_1
    proposal: 0.1

  bias_DECALS__3_b1:
    prior: 
        min: 0.8
        max: 3.0
    ref:
      dist: norm
      loc: 1.74
      scale: 0.1
    latex: b^{DECALS_3}_1
    proposal: 0.1

  # eBOSS-QSOs
  bias_eBOSS__0_b1:
    prior: 
        min: 0.8
        max: 5.0
    ref:
      dist: norm
      loc: 2.27
      scale: 0.1
    latex: b^{eBOSS_0}_1
    proposal: 0.1

  bias_eBOSS__1_b1:
    prior: 
        min: 0.8
        max: 5.0
    ref:
      dist: norm
      loc: 2.45
      scale: 0.1
    latex: b^{eBOSS_1}_1
    proposal: 0.1

  # Magnification biases
  bias_eBOSS__0_s: 0.2
  bias_eBOSS__1_s: 0.2
 
  # In this paper, the same IA was used for all wl samples
  bias_A_IA:
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: 0.42
      scale: 0.1
    latex: A_{IA}
    proposal: 0.1

  limber_eta_IA: 
    prior: 
      min: -5
      max: 5
    ref:
      dist: norm
      loc: -0.63
      scale: 0.1
    latex: \eta_{IA}
    proposal: 0.1

# CCL settings
theory:
  cl_like.CCL_BLCDM:
    nonlinear_model: muSigma
    classy_arguments:
      Omega_Lambda: 0
      Omega_fld: 0
      Omega_smg: -1
      non linear: hmcode
      gravity_model: mgclass_fs
      expansion_model: lcdm
      use_Sigma: "yes"  # Quotes needed. Yes in YAML = True
      expansion_smg: 0.7    # DE, tuned. To be moved to parameters if sampled
      P_k_max_1/Mpc: 100
      z_max_pk: 6
      output: "tCl, lCl, pCl, mPk"
      lensing: "yeah"
      # From MontePython for Planck
      # The base model features two massless and one massive neutrino with
      # m=0.06eV.  The settings below ensures that Neff=3.046 # and m/omega =
      # 93.14 eV
      N_ur: 2.0328
      N_ncdm: 1
      m_ncdm: 0.06
      T_ncdm: 0.71611
  cl_like.Pk:
    # Linear, EulerianPT, LagrangianPT
    bias_model: "Linear"
  cl_like.Limber:
    nz_model: "NzShift"
    ia_model: "IADESY1"
    input_params_prefix: "limber"
  cl_like.ClFinal:
    input_params_prefix: "bias"
    shape_model: ShapeMultiplicative

# Likelihood settings
likelihood:
  planck_2018_lowl.TT: 
  planck_2018_lowl.EE: 
  planck_2018_highl_plik.TTTEEE: 
  planck_2018_lensing.clik:
  cl_like.ClLike:
    # Input sacc file
    input_file: /mnt/extraspace/gravityls_3/sacc_files/cls_FD_covG_dzMarg_mMarg.fits
    # List all relevant bins. The clustering
    # bins are clX, the shear bins are shX.
    bins:
      - name: DESgc__0
      - name: DESgc__1
      - name: DESgc__2
      - name: DESgc__3
      - name: DESgc__4
      - name: DESwl__0
      - name: DESwl__1
      - name: DESwl__2
      - name: DESwl__3
      - name: DECALS__0
      - name: DECALS__1
      - name: DECALS__2
      - name: DECALS__3
      - name: KiDS1000__0
      - name: KiDS1000__1
      - name: KiDS1000__2
      - name: KiDS1000__3
      - name: KiDS1000__4
      - name: eBOSS__0
      - name: eBOSS__1
      - name: PLAcv

    # List all 2-points that should go into the
    # data vector. For now we only include
    # galaxy-galaxy auto-correlations, but all
    # galaxy-shear and shear-shear correlations.
    twopoints:
      # SD (DESgc, DESwl, PlAcv)
      - bins: [DESgc__0, DESgc__0]
      - bins: [DESgc__1, DESgc__1]
      - bins: [DESgc__2, DESgc__2]
      - bins: [DESgc__3, DESgc__3]
      - bins: [DESgc__4, DESgc__4]

      - bins: [DESgc__0, DESwl__0]
      - bins: [DESgc__0, DESwl__1]
      - bins: [DESgc__0, DESwl__2]
      - bins: [DESgc__0, DESwl__3]
        
      - bins: [DESgc__1, DESwl__0]
      - bins: [DESgc__1, DESwl__1]
      - bins: [DESgc__1, DESwl__2]
      - bins: [DESgc__1, DESwl__3]

      - bins: [DESgc__2, DESwl__0]
      - bins: [DESgc__2, DESwl__1]
      - bins: [DESgc__2, DESwl__2]
      - bins: [DESgc__2, DESwl__3]

      - bins: [DESgc__3, DESwl__0]
      - bins: [DESgc__3, DESwl__1]
      - bins: [DESgc__3, DESwl__2]
      - bins: [DESgc__3, DESwl__3]

      - bins: [DESgc__4, DESwl__0]
      - bins: [DESgc__4, DESwl__1]
      - bins: [DESgc__4, DESwl__2]
      - bins: [DESgc__4, DESwl__3]

      - bins: [DESwl__0, DESwl__0]
      - bins: [DESwl__0, DESwl__1]
      - bins: [DESwl__0, DESwl__2]
      - bins: [DESwl__0, DESwl__3]
      - bins: [DESwl__1, DESwl__1]
      - bins: [DESwl__1, DESwl__2]
      - bins: [DESwl__1, DESwl__3]
      - bins: [DESwl__2, DESwl__2]
      - bins: [DESwl__2, DESwl__3]
      - bins: [DESwl__3, DESwl__3]

      - bins: [DESgc__0, PLAcv]
      - bins: [DESgc__1, PLAcv]
      - bins: [DESgc__2, PLAcv]
      - bins: [DESgc__3, PLAcv]
      - bins: [DESgc__4, PLAcv]

      - bins: [DESwl__0, PLAcv]
      - bins: [DESwl__1, PLAcv]
      - bins: [DESwl__2, PLAcv]
      - bins: [DESwl__3, PLAcv]
        
      # ND (DECALS, KiDS1000, PlAcv)
      - bins: [DECALS__0, DECALS__0]
      - bins: [DECALS__1, DECALS__1]
      - bins: [DECALS__2, DECALS__2]
      - bins: [DECALS__3, DECALS__3]

      - bins: [DECALS__0, KiDS1000__0]
      - bins: [DECALS__0, KiDS1000__1]
      - bins: [DECALS__0, KiDS1000__2]
      - bins: [DECALS__0, KiDS1000__3]
      - bins: [DECALS__0, KiDS1000__4]
        
      - bins: [DECALS__1, KiDS1000__0]
      - bins: [DECALS__1, KiDS1000__1]
      - bins: [DECALS__1, KiDS1000__2]
      - bins: [DECALS__1, KiDS1000__3]
      - bins: [DECALS__1, KiDS1000__4]

      - bins: [DECALS__2, KiDS1000__0]
      - bins: [DECALS__2, KiDS1000__1]
      - bins: [DECALS__2, KiDS1000__2]
      - bins: [DECALS__2, KiDS1000__3]
      - bins: [DECALS__2, KiDS1000__4]

      - bins: [DECALS__3, KiDS1000__0]
      - bins: [DECALS__3, KiDS1000__1]
      - bins: [DECALS__3, KiDS1000__2]
      - bins: [DECALS__3, KiDS1000__3]
      - bins: [DECALS__3, KiDS1000__4]

      - bins: [KiDS1000__0, KiDS1000__0]
      - bins: [KiDS1000__0, KiDS1000__1]
      - bins: [KiDS1000__0, KiDS1000__2]
      - bins: [KiDS1000__0, KiDS1000__3]
      - bins: [KiDS1000__0, KiDS1000__4]
      - bins: [KiDS1000__1, KiDS1000__1]
      - bins: [KiDS1000__1, KiDS1000__2]
      - bins: [KiDS1000__1, KiDS1000__3]
      - bins: [KiDS1000__1, KiDS1000__4]
      - bins: [KiDS1000__2, KiDS1000__2]
      - bins: [KiDS1000__2, KiDS1000__3]
      - bins: [KiDS1000__2, KiDS1000__4]
      - bins: [KiDS1000__3, KiDS1000__3]
      - bins: [KiDS1000__3, KiDS1000__4]
      - bins: [KiDS1000__4, KiDS1000__4]

      - bins: [DECALS__0, PLAcv]
      - bins: [DECALS__1, PLAcv]
      - bins: [DECALS__2, PLAcv]
      - bins: [DECALS__3, PLAcv]

      - bins: [KiDS1000__0, PLAcv]
      - bins: [KiDS1000__1, PLAcv]
      - bins: [KiDS1000__2, PLAcv]
      - bins: [KiDS1000__3, PLAcv]
      - bins: [KiDS1000__4, PLAcv]

      # eBOSS-QSOs
      - bins: [eBOSS__0, eBOSS__0]
      - bins: [eBOSS__1, eBOSS__1]

      - bins: [eBOSS__0, PLAcv]
      - bins: [eBOSS__1, PLAcv]

    defaults:
      # Scale cut for galaxy clustering
      # (ignored for shear-shear)
      kmax: 0.15
      # These ones will apply to all power
      # spectra (unless the lmax corresponding
      # to the chosen kmax is smaller).
      lmin: 0
      lmax: 2000

      # Note: as is now, we will be removing the DESgc x DESwl Cell at ell<30.
      # In the paper, those were kept and only removed for wl x wl wl x PlAcv.
      DESwl__0:
        lmin: 30
      DESwl__1:
        lmin: 30
      DESwl__2:
        lmin: 30
      DESwl__3:
        lmin: 30

      DECALS__0:
        lmin: 30
      DECALS__1:
        lmin: 30
      DECALS__2:
        lmin: 30
      DECALS__3:
        lmin: 30
      KiDS1000__0:
        lmin: 100
      KiDS1000__1:
        lmin: 100
      KiDS1000__2:
        lmin: 100
      KiDS1000__3:
        lmin: 100
      KiDS1000__4:
        lmin: 100

      # For eBOSS we chose kmin = 0.02Mpc^-1 and estimated the 
      # lmin ~ chi(z_mean) * kmin, with z_mean = {1.2, 1.8}
      eBOSS__0:
        lmin: 79
        mag_bias: True
      eBOSS__1:
        lmin: 102
        mag_bias: True

package_path: '~/codes/cobaya_packages'
debug: False
stop_at_error: False
output: 'chains/FD_Garcia-Garcia2021_dzMarg_mMarg_P18_mgclass_fs_lcdm_nu/FD_Garcia-Garcia2021_dzMarg_mMarg_P18_mgclass_fs_lcdm_nu'
