# Configuration to use the Full Dataset (FD) from Garcia-Garcia et al. 2021
# (2105.12108), but with the dz and multiplicative biases marginalized into the
# covariance.

sampler:
  mcmc:
    burn_in: 0
    max_tries: 1000
    drag: True
    covmat: './chains/P18_lcdm_classy_nu/P18_lcdm_classy_nu.covmat'

params:
  # BLCDM parameters
  # mu
  parameters_smg__1: 0

  # Sigma
  parameters_smg__2: 0

  # Cosmological parameters
  A_sE9:
    prior:
      min: 0.5
      max: 5.
    ref:
      dist: norm
      loc: 2.02
      scale: 0.01
    latex: 10^9 A_s
    proposal: 0.001
    drop: True

  # Class does not understand A_sE9
  A_s:
    value: 'lambda A_sE9: A_sE9 * 1e-9'
    latex: A_s

  Omega_cdm:
    prior: 
      min: 0.07
      max: 0.83
    ref:
      dist: norm
      loc: 0.2
      scale: 0.01
    latex: \Omega_c
    proposal: 0.01

  Omega_b:
    prior: 
      min: 0.03
      max: 0.07
    ref:
      dist: norm
      loc: 0.04
      scale: 0.001
    latex: \Omega_b
    proposal: 0.001

  h: 
    prior: 
      min: 0.55
      max: 0.91
    ref:
      dist: norm
      loc: 0.7
      scale: 0.02
    latex: h
    proposal: 0.02

  n_s:
    prior: 
      min: 0.87
      max: 1.07
    ref:
      dist: norm
      loc: 0.96
      scale: 0.02
    latex: n_s
    proposal: 0.02

  tau_reio:
    prior:
      min: 0.004
      max: 1.000
    ref:
      dist: norm
      loc: 0.054
      scale: 0.008

  sigma8:
    latex: \sigma_8

  S8:
    latex: S_8

# CCL settings
theory:
  cl_like.CCL_BLCDM:
    nonlinear_model: muSigma
    classy_arguments:
      Omega_Lambda: 0
      Omega_fld: 0
      Omega_smg: -1
      non linear: hmcode
      gravity_model: mgclass_fs
      expansion_model: lcdm
      use_Sigma: "yes"  # Quotes needed. Yes in YAML = True
      expansion_smg: 0.7    # DE, tuned. To be moved to parameters if sampled
      P_k_max_1/Mpc: 1
      output: "tCl, lCl, pCl, mPk"
      lensing: "yeah"
      # From MontePython
      # The base model features two massless and one massive neutrino with
      # m=0.06eV.  The settings below ensures that Neff=3.046 # and m/omega =
      # 93.14 eV
      N_ur: 2.0328
      N_ncdm: 1
      m_ncdm: 0.06
      T_ncdm: 0.71611

# Likelihood settings
likelihood:
  planck_2018_lowl.TT: 
  planck_2018_lowl.EE: 
  planck_2018_highl_plik.TTTEEE: 
  planck_2018_lensing.clik:

package_path: '~/codes/cobaya_packages'
debug: False
stop_at_error: False
output: 'chains/P18_lcdm_nu/P18_lcdm_nu'
